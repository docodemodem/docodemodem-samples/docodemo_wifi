# どこでもでむのWifiを使ったサンプルプログラムです

サンプルプログラム[sample_m0](https://gitlab.com/docodemodem/docodemodem-samples/sample_m0)から届くセンサー値をGoogle Spread Sheetに保存します。


GSSは[こちら](https://docs.google.com/spreadsheets/d/1d033v5Tq-dDD8tIA76uh79WQYT2J3nDbm4m4cMhuZH4/edit#gid=0)からアクセスできます。



HTTPのリダイレクト処理に[こちらのコード](https://github.com/electronicsguy/HTTPSRedirect)を利用しております。

HTTPSRedirect.cppの98行あたりのコメントアウトを外して使用します。  
`//stop(); -> stop();`

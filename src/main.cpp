/*
 * Sample program for DocodeModem
 * Copyright (c) 2023 Circuit Desgin,Inc
 * Released under the MIT license
 */
/*
Use for http redirect.
https://github.com/electronicsguy/HTTPSRedirect

change line98 of HTTPSRedirect.cpp:  //stop(); -> stop();
*/
#include <docodemo.h>
#include <SlrModem.h>
#include <Wifi.h>
#include "HTTPSRedirect.h"

#define CMD_HEADER 0x01

const char *ssid = "*********";
const char *password = "**********";

const char *host = "script.google.com";
const int httpsPort = 443;

// gss   https://docs.google.com/spreadsheets/d/1d033v5Tq-dDD8tIA76uh79WQYT2J3nDbm4m4cMhuZH4/edit#gid=0
// macro https://script.google.com/macros/s/AKfycbzLmykz_fhnWO7NeWxi4k9RzBaCq3XPOdgJwYB8iS554Q3S4tLGsqplBIX_Oucn8tqgxg/exec

const String _MACRO_URL = "/macros/s/AKfycbzLmykz_fhnWO7NeWxi4k9RzBaCq3XPOdgJwYB8iS554Q3S4tLGsqplBIX_Oucn8tqgxg/exec";

const uint8_t DEVICE_EI = 0x01;
const uint8_t DEVICE_GI = 0x02;
const uint8_t DEVICE_DI = 0x00; // 00 is boradcast
const uint8_t CHANNEL = 0x10;

DOCODEMO Dm;
HardwareSerial UartModem(MODEM_UART_NO);
SlrModem modem;

void wifi_init()
{
  WiFi.disconnect();
  // WiFi.mode(WIFI_OFF);
  delay(100);

  int retryCnt = 0;
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    SerialDebug.print(".");
    delay(1000);
    retryCnt++;
    if (retryCnt > 5)
    {
      WiFi.begin(ssid, password);
      retryCnt = 0;
    }
  }
  SerialDebug.print("ipaddress:");
  SerialDebug.println(WiFi.localIP());
}

static xTaskHandle radio_task_handle;
static void radio_task(void *val)
{
  Dm.ModemPowerCtrl(ON);
  delay(150);

  UartModem.begin(19200, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);
  while (!UartModem)
    ;
  modem.Init(UartModem, nullptr);

  // if you turn off radio power, set again or select true to save setting.
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);

  Dm.LedCtrl(GREEN_LED, ON);

  while (1)
  {
    modem.Work();

    if (modem.HasPacket())
    {
      Dm.LedCtrl(RED_LED, ON);
      SerialDebug.print("Received packet:");
      
      int16_t rssi{0};
      const uint8_t *pData;
      uint8_t len{0};
      modem.GetPacket(&pData, &len);
      modem.GetRssiLastRx(&rssi);

      if (len == 24) // size check
      {
        float recvdata[6];
        memcpy(&recvdata[0], pData, len);

        // simple packet header check
        uint8_t cmd = (((int)recvdata[0]) >> 8) & 0xff;
        if (cmd == CMD_HEADER)
        {
          SerialDebug.printf(" %.1fm/S, %.1fC, %.1fhPa, %.1f, env rssi: %d, rcv rssi: %d\r\n", recvdata[1], recvdata[2], recvdata[3], recvdata[4], (int)recvdata[5], rssi);

          String payload = "1=" + String(recvdata[1], 1);
          payload += "&2=" + String(recvdata[2], 1);
          payload += "&3=" + String(recvdata[3], 1);
          payload += "&4=" + String(recvdata[4]);
          payload += "&5=" + String(recvdata[5]);
          payload += "&6=" + String(rssi);

          SerialDebug.println("payload: " + payload);
          HTTPSRedirect *client = new HTTPSRedirect(httpsPort);
          client->setInsecure();

          //client->setPrintResponseBody(true);
          // client->setContentTypeHeader("application/json");

          if (client->connect(host, httpsPort))
          {
            Serial.println("connected");

            client->POST(_MACRO_URL, host, payload);
            Serial.print("Status      :");
            Serial.println(client->getStatusCode());
            Serial.print("reasonPhrase:");
            Serial.println(client->getReasonPhrase());
            Serial.print("body        :");
            Serial.println(client->getResponseBody());
          }
          else
          {
            Serial.println("not connect");
            wifi_init();
          }

          delete client;
        }
        else
        {
          SerialDebug.println("Not our packet...");
          memset(&recvdata[0], 0, len);
        }
      }

      modem.DeletePacket();
      Dm.LedCtrl(RED_LED, OFF);
    }
  }
  vTaskDelete(NULL);
}

void setup()
{
  SerialDebug.begin(115200);
  Dm.begin();

  wifi_init();
  
  xTaskCreatePinnedToCore(radio_task, "radio_task", 8192, NULL, 2, &radio_task_handle, 1);

  vTaskDelete(NULL);
}

void loop()
{
  // Never get here
}
